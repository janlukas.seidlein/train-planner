package de.uniwue.jpp.trains;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class RoutePlanner {
	ArrayList<Connection> connections; // Kopie der Listen eventuell uebernehmen
	ArrayList<TrainStation> trainStations;
	
	// Connection prev ;

	public RoutePlanner(List<Connection> connections, List<TrainStation> trainStations) {
		
		
		Collections.sort(connections, new Comparator<Connection>() {
			@Override
			public int compare(Connection connections2, Connection connections1) {
				return connections2.compareTo(connections1);
			}
		});
		System.out.println(connections);
		this.connections = new ArrayList<Connection>(connections);
		this.trainStations = new ArrayList<TrainStation>(trainStations);
		//swap();
	}

	public List<TrainStation> getTrainStations() {
		return trainStations;
	}

	public List<Connection> getConnections() {
		return connections;
	}

	public void addConnection(Connection connection) {
		connections.add(connection);
		Collections.sort(connections, new Comparator<Connection>() {
			@Override
			public int compare(Connection connections2, Connection connections1) {
				return connections2.compareTo(connections1);
			}
		});
	}

	public void addTrainStation(TrainStation station) {
		if (!trainStations.contains(station))

			trainStations.add(station);// TODO: Sortierung beim Einfuegen
										// gewaerleisten ?

		Collections.sort(trainStations, new Comparator<TrainStation>() {
			@Override
			public int compare(TrainStation s2, TrainStation s1) {
				return s1.compareTo(s2);
			}
		});

	}

	public TrainStation getStationWithName(String name) {
		for (TrainStation t : trainStations) {
			if (t.getName() == name)
				return t;
		}
		return null;
	}
	public void swap(){
		List<Connection> a = new ArrayList<Connection>();
		a.addAll(connections);
		connections.clear();
		for(int i =a.size()-1;i>0;i--){
			connections.add(a.get(i));
		}
		}

	public List<Connection> calculateRoute(Connection compositeConnection) {
		
		
		//System.out.println(connections);
		List<Connection> result = new ArrayList<Connection>();
            Connection tmp =null;
           // Collections.sort(connections) ;
   
		for (Connection c : connections) {

			if ((c.getArrivingStation().equals(compositeConnection.getArrivingStation()))
					&& (c.getDepartingStation().equals(compositeConnection.getDepartingStation()))) {
				result.add(c);
				// System.out.println("Direktverbindung");
				return result;

			}//else{result.add(c);    }
			
			//result.add(c);
			//tmp = c;
            
		}
		// result.add(tmp);

		for (Connection d : connections) {
			if (compositeConnection.getDepartingStation().equals(d.getDepartingStation())) {
				
				Connection umsteige = new Connection(d.getArrivingStation(), compositeConnection.getArrivingStation(), d.getArrival(),
						compositeConnection.getArrival());
				
				result.add(d);
				//System.out.println("Rekursionshinzufuegen");
			
				List<Connection> k = calculateRoute(umsteige);
				
				if(k.size() == 0){
					result.remove(d);
					
				}else {
					result.addAll(k);
					return result;
				}
				if( (!compositeConnection.getArrivingStation().equals(d.getArrivingStation())  ) && d.getArrivingStation().isEnd()   ){
	                  for(Connection leer: result){
	                	  result.remove(leer);
	                	  
	                  }
			}
			
			
                  
			}
		} 
		System.out.println(result);
		return new ArrayList() ;
	}
}
