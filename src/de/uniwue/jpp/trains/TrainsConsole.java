package de.uniwue.jpp.trains;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Scanner;
import java.util.regex.MatchResult;

public class TrainsConsole {
	
	RoutePlanner planner;
	public TrainsConsole(RoutePlanner planner){
		this.planner = planner;
	}
	public void start(InputStream in,OutputStream out){
		PrintStream ps = new PrintStream(System.out);
		ps.println("Route planning system started.");
		MenueAusgabe();
		int input;
		Scanner sc = new Scanner(System.in);
		if(sc.hasNextInt()){
		 input = sc.nextInt();
		}else input = 1;
		ps.println("");
		
		if(input == 1){
			List<TrainStation> c = planner.getTrainStations();
			for(TrainStation co:c){
				ps.println(co.getName());
			}
			
			MenueAusgabe();
		}
		else if(input == 2){
			ps.println("Available Direct Connections:");
			List<Connection> c = planner.getConnections();
			for(Connection co:c){
				ps.println(co.toString());
			}
			//TODO
			
			MenueAusgabe();
		}
		else if(input == 3){
			Scanner sc1 = new Scanner(System.in);
			ps.println("Find a Connection:");
			ps.println("Departing Station:");
			String departingStation = sc1.nextLine();
			Scanner sc2 = new Scanner(System.in);
			ps.println("Destination Station:");
			String destinationStation = sc2.nextLine();
			ps.println("Departure after:");
			//TODO: Korrektes einlesen der LocalDateTime!
			Scanner sc3 = new Scanner(System.in);
			
			sc3.findInLine("(\\d\\d)\\.(\\d\\d)\\.(\\d\\d\\d\\d);(\\d\\d):(\\d\\d)");
			try{
				MatchResult mr = sc3.match();
				int day = Integer.parseInt(mr.group(1));
				int month = Integer.parseInt(mr.group(2));
				int year = Integer.parseInt(mr.group(3));
				int hour = Integer.parseInt(mr.group(4));
				int minute = Integer.parseInt(mr.group(5));
				LocalDateTime departureTime = LocalDateTime.of(year, month,day,hour,minute );
				
			}catch (IllegalStateException e) {
				System.err.println("Invalid input !");
			}
			
			ps.println("Optimal Connection:");
			//Connection c = new Connection(departingStation,destinationStation,,);
			MenueAusgabe();
		}
		else if(input == 4){
			//terminate();
		}
		else{
		      ps.println("Error: Input wasnt a integer between 1 and 4.");
		}
	}
	public void MenueAusgabe(){
		PrintStream ps = new PrintStream(System.out);
		ps.println("");
		ps.println("1. Show all Stations");
		ps.println("2. Show all direct connections");
		ps.println("3. Find a Connection");
		ps.println("4. Quit");
		ps.println("Select an Option");
		
	}

}
