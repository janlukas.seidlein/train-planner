package de.uniwue.jpp.trains;

import java.util.Objects;

public class TrainStation implements Comparable<TrainStation> {
	private final String name;
	private final boolean end;
	public TrainStation(String name,boolean end){
		this.name = name;
		this.end = end;
	}
	public String getName(){
		return name;
		
	}
	public boolean isEnd(){
		if(end){return true;}
		return false;
	}
	public boolean equals(Object other){
		if(other.toString() == this.name){
			return true;
		}
		else return false;
	}
	public int hashCode(){
		
		return Objects.hashCode(this.name);
		
	}
	public String toString(){
		return name;
	}
	public int compareTo(TrainStation o){
		return name.compareTo(o.getName());
	}
	

}
